import logging
import os
import pathlib
import sys

import pytest

log = logging.getLogger("idem.tests")


def pytest_runtest_protocol(item, nextitem):
    """
    implements the runtest_setup/call/teardown protocol for
    the given test item, including capturing exceptions and calling
    reporting hooks.
    """
    log.debug(f">>>>> START >>>>> {item.name}")


def pytest_runtest_teardown(item):
    """
    called after ``pytest_runtest_call``
    """
    log.debug(f"<<<<< END <<<<<<< {item.name}")


@pytest.fixture
def os_sleep_secs():
    if "CI_RUN" in os.environ:
        return 1.75
    return 0.5


@pytest.fixture
def code_dir() -> pathlib.Path:
    base = pathlib.Path(__file__).parent.parent.absolute()

    if base in sys.path:
        sys.path.remove(base)

    sys.path.insert(0, base)
    return base


@pytest.fixture
def tree(code_dir) -> str:
    return str(code_dir.joinpath(code_dir, "tests", "sls"))


@pytest.fixture
def runpy(code_dir) -> str:
    return str(code_dir.joinpath("run.py"))
