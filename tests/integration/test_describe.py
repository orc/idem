import json
import subprocess
import sys
import tempfile

import pop.hub


def test_cli(runpy):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")

    # Run the describe command
    cmd = [sys.executable, runpy, "describe", "test", "--output=json"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    assert ret.returncode == 0, ret.stderr
    yaml = ret.stdout.decode()

    fh = tempfile.NamedTemporaryFile("w+", suffix=".sls")
    fh.write(yaml)
    fh.flush()

    # Verify that passing states were created from it
    cmd = [sys.executable, runpy, "state", fh.name, "--output=json", "--runtime=serial"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    fh.close()
    data = json.loads(ret.stdout.decode())
    assert ret.returncode == 0, ret.stderr
    assert data == {
        "test_|-Description of test.anop_|-anop_|-anop": {
            "__run_num": 1,
            "changes": {},
            "comment": "Success!",
            "name": "anop",
            "result": True,
        },
        "test_|-Description of test.configurable_test_state_|-configurable_test_state_|-configurable_test_state": {
            "__run_num": 2,
            "changes": {
                "testing": {
                    "new": "Something " "pretended " "to " "change",
                    "old": "Unchanged",
                }
            },
            "comment": "",
            "name": "configurable_test_state",
            "result": True,
        },
        "test_|-Description of test.describe_|-Description of test.describe_|-describe": {
            "Description of test.anop": {"test.anop": [{"name": "anop"}]},
            "Description of test.configurable_test_state": {
                "test.configurable_test_state": [
                    {"name": "configurable_test_state"},
                    {"changes": True},
                    {"result": True},
                    {"comment": ""},
                ]
            },
            "Description of test.describe": {"test.describe": []},
            "Description of test.fail_with_changes": {
                "test.fail_with_changes": [{"name": "fail_with_changes"}]
            },
            "Description of test.fail_without_changes": {
                "test.fail_without_changes": [{"name": "fail_without_changes"}]
            },
            "Description of test.mod_watch": {
                "test.mod_watch": [{"name": "mod_watch"}]
            },
            "Description of test.none_without_changes": {
                "test.none_without_changes": [{"name": "none_without_changes"}]
            },
            "Description of test.nop": {"test.nop": [{"name": "nop"}]},
            "Description of test.succeed_with_changes": {
                "test.succeed_with_changes": [{"name": "succeed_with_changes"}]
            },
            "Description of test.succeed_with_comment": {
                "test.succeed_with_comment": [
                    {"name": "succeed_with_comment"},
                    {"comment": None},
                ]
            },
            "Description of test.succeed_without_changes": {
                "test.succeed_without_changes": [{"name": "succeed_without_changes"}]
            },
            "Description of test.treq": {"test.treq": [{"name": "treq"}]},
            "Description of test.update_low": {
                "test.update_low": [{"name": "update_low"}]
            },
            "__run_num": 3,
        },
        "test_|-Description of test.fail_with_changes_|-fail_with_changes_|-fail_with_changes": {
            "__run_num": 4,
            "changes": {
                "testing": {
                    "new": "Something " "pretended " "to " "change",
                    "old": "Unchanged",
                }
            },
            "comment": "Failure!",
            "name": "fail_with_changes",
            "result": False,
        },
        "test_|-Description of test.fail_without_changes_|-fail_without_changes_|-fail_without_changes": {
            "__run_num": 5,
            "changes": {},
            "comment": "Failure!",
            "name": "fail_without_changes",
            "result": False,
        },
        "test_|-Description of test.mod_watch_|-mod_watch_|-mod_watch": {
            "__run_num": 6,
            "changes": {"watch": True},
            "comment": "Watch " "ran!",
            "name": "mod_watch",
            "result": True,
        },
        "test_|-Description of test.none_without_changes_|-none_without_changes_|-none_without_changes": {
            "__run_num": 7,
            "changes": {},
            "comment": "Success!",
            "name": "none_without_changes",
            "result": None,
        },
        "test_|-Description of test.nop_|-nop_|-nop": {
            "__run_num": 8,
            "changes": {},
            "comment": "Success!",
            "name": "nop",
            "result": True,
        },
        "test_|-Description of test.succeed_with_changes_|-succeed_with_changes_|-succeed_with_changes": {
            "__run_num": 9,
            "changes": {
                "testing": {
                    "new": "Something " "pretended " "to " "change",
                    "old": "Unchanged",
                }
            },
            "comment": "Success!",
            "name": "succeed_with_changes",
            "result": True,
        },
        "test_|-Description of test.succeed_with_comment_|-succeed_with_comment_|-succeed_with_comment": {
            "__run_num": 10,
            "changes": {},
            "comment": None,
            "name": "succeed_with_comment",
            "result": True,
        },
        "test_|-Description of test.succeed_without_changes_|-succeed_without_changes_|-succeed_without_changes": {
            "__run_num": 11,
            "changes": {},
            "comment": "Success!",
            "name": "succeed_without_changes",
            "result": True,
        },
        "test_|-Description of test.treq_|-treq_|-treq": {
            "__run_num": 14,
            "changes": {},
            "comment": "Success!",
            "name": "treq",
            "result": True,
        },
        "test_|-Description of test.update_low_|-update_low_|-update_low": {
            "__run_num": 12,
            "changes": {},
            "comment": "Success!",
            "name": "update_low",
            "result": True,
        },
        "test_|-king_arthur_|-totally_extra_alls_|-nop": {
            "__run_num": 13,
            "changes": {},
            "comment": "Success!",
            "name": "totally_extra_alls",
            "result": True,
        },
    }
