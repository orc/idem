import pop.hub
import pop.loader


async def test_contracts_separated_resolver(capsys):
    # Set up the hub like idem does
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")

    def resolve(path, context):
        return hub.exec.test.ctx

    hub.pop.sub.dynamic(resolve, None, dyne_name="mod", sub=hub.exec)

    ret = await hub.idem.ex.run("exec.mod.foo.bar", args=[], kwargs={})
    assert ret.result
    assert ret.ret == {"acct": {}}
    assert "idem.exec.test" in ret.ref
