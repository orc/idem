import json
import tempfile
from unittest import mock

import pop.hub
import pytest


@pytest.fixture(scope="module", name="hub")
def idem_hub():
    hub = pop.hub.Hub()
    hub.pop.loop.create()
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture(scope="module")
def cred_file():
    with tempfile.NamedTemporaryFile("w+") as creds:
        creds.write("foo:\n- bar")
        creds.flush()
        yield creds.name


@pytest.fixture(scope="module")
def acct_file():
    with tempfile.NamedTemporaryFile() as f:
        yield f.name


@pytest.mark.asyncio
async def test_encrypt(hub, acct_key, cred_file, acct_file, capsys):
    with mock.patch(
        "sys.argv",
        [
            "idem",
            "encrypt",
            cred_file,
            f"--acct-key='{acct_key}'",
            f"--output-file={acct_file}",
            "--crypto-plugin=fernet",
        ],
    ):
        hub.pop.config.load(["idem", "acct", "rend"], cli="idem")

    retcode = await hub.idem.init.cli_apply()
    assert retcode == 0

    stdout, stderr = capsys.readouterr()
    assert not stderr, stderr

    with open(acct_file) as fh:
        assert fh.read()


@pytest.mark.asyncio
async def test_decrypt(hub, acct_key, acct_file, capsys):
    with mock.patch(
        "sys.argv",
        [
            "idem",
            "decrypt",
            acct_file,
            f"--acct-key='{acct_key}'",
            "--crypto-plugin=fernet",
            "--output=json",
        ],
    ):
        hub.pop.config.load(["idem", "acct", "rend"], cli="idem")

    retcode = await hub.idem.init.cli_apply()
    assert retcode == 0

    stdout, stderr = capsys.readouterr()
    assert not stderr, stderr
    assert json.loads(stdout) == {"foo": ["bar"]}, stderr
